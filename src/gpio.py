from enum import Enum

try:
    import RPi.GPIO as GPIO
except RuntimeError as ex:
    import mock_gpio as GPIO
    print("GPIO library could not be imported. Mock library will be used.")
except ImportError as ex:
    import mock_gpio as GPIO
    print("GPIO library could not be imported. Mock library will be used.")

GPIO.setmode(GPIO.BCM)

OUTPUT_GPIOS = []


class GPIOValue(Enum):
    HIGH = 'high'
    LOW = 'low'


def set_gpio_to(gpio_id: int, value: GPIOValue):
    gpio_to_output(gpio_id)
    if value is GPIOValue.HIGH:
        GPIO.output(gpio_id, 1)
    elif value is GPIOValue.LOW:
        GPIO.output(gpio_id, 0)


def gpio_to_output(gpio_id: int):
    if gpio_id not in OUTPUT_GPIOS:
        OUTPUT_GPIOS.append(gpio_id)
        GPIO.setup(gpio_id, GPIO.OUT)
