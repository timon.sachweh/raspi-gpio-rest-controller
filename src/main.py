import os

from flask import Flask, jsonify
import time

from gpio import GPIOValue, set_gpio_to

app = Flask("RestSensorData")
app.config.from_object(os.getenv('APP_SETTINGS', "config.DevelopmentConfig"))


@app.route('/test', methods=['GET'])
def test():
    return "This is a hello world test"


@app.route('/gpio/<gpio_id>/<value>', methods=['GET', 'PUT', 'POST'])
def gpio_set_high(gpio_id, value):
    """
    Method for exposing a rest controller, that consumes requests for setting the pins values from high to low (gpio)

    :param gpio_id: Id of the gpio pin
    :param value: The value, which should be set (either high, or low)
    """
    try:
        gpio_id = int(gpio_id)
        set_gpio_to(gpio_id, GPIOValue(value))
        jsonFormat = jsonify(
            timestamp=int(time.time()),
            gpio_id=gpio_id,
            value=value
        )
    except ValueError as ex:
        jsonFormat = jsonify(
            timestamp=int(time.time()),
            errorTitle="ValueError",
            errorDescription="You must either specify high, or low as value parameter for this endpoint",
            error=str(ex)
        )
    return jsonFormat


if __name__ == '__main__':
    app.run(host='0.0.0.0')
