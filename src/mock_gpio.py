
BOARD = "Mock-board"
OUT = "output-pin"


def setmode(mode):
    print(mode)


def output(gpio_id, value):
    print("GPIO: gpio_id={}, value={}".format(gpio_id, value))


def setup(gpio_id, mode):
    print("Setup: gpio_id={}, mode={}".format(gpio_id, mode))
