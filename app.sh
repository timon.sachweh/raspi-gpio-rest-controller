#!/bin/bash

echo "Starting the http service for serving the rest service"

export APP_SETTINGS="config.ProductionConfig" && cd src && gunicorn --bind 0.0.0.0:80 main:app