all:
	@$(MAKE) requirements
	@$(MAKE) copy-systemd-script
	@$(MAKE) copy-program
	@$(MAKE) enable-systemd
	@$(MAKE) systemd-start

requirements:
	sudo -H pip3 install -r requirements.txt
	sudo -H pip install -r requirements.txt

copy-systemd-script:
	cp ./gpio-rest-controller.service /etc/systemd/system/gpio-rest-controller.service

copy-program:
	cp -r . /opt/raspi-gpio-rest-controller

enable-systemd:
	systemctl daemon-reload
	systemctl enable gpio-rest-controller.service

remove-temp-folder:
	cd .. && rm -r raspi-gpio-rest-controller

systemd-start:
	systemctl start gpio-rest-controller.service

systemd-stop:
	systemctl stop gpio-rest-controller.service

start:
	@$(MAKE) start-prod

start-dev:
	python3 ./src/main.py

start-prod:
	export APP_SETTINGS="config.ProductionConfig" && cd src && gunicorn --bind 0.0.0.0:80 main:app &