# Rest service for controlling gpio pins on a raspberry pi

## Installation

To install this program, you have to enter ```sudo make``` in the root directory
of the program.
1. The neccessary requirements will be installed
2. systemd script will be copied
3. move program from current directory to /opt/raspi-gpio-rest-controller
4. systemd script will be enabled
5. systemd script will be started

## Start and Stop the program
The service could be started by running ```sudo systemctl start gpio-rest-controller```.
Stopping the service is able by running ```sudo systemctl stop gpio-rest-controller```.